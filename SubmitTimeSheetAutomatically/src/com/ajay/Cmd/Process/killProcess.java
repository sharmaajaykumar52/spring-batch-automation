package com.ajay.Cmd.Process;

import java.io.IOException;
import java.util.Scanner;

public final class killProcess {

	public final void killCmdProcess(String... taskNameArray) {
		try {
			for (String taskName : taskNameArray) {
				Process processObj = Runtime.getRuntime().exec("taskkill /F /IM " + taskName + " /T");
				// processObj.destroy();
				Scanner scanerObj = new Scanner(processObj.getInputStream());
				System.out.println("Displaying the Output : - ");
				while (scanerObj.hasNextLine()) {
					System.out.println(scanerObj.nextLine());
				}
				scanerObj.close();
			}
		} catch (IOException e) {
			System.out.println("Error Occured While running taskill " + e.getMessage());
		}
	}
}
