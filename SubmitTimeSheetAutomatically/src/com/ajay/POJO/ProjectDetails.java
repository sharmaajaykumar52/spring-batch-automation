package com.ajay.POJO;

public class ProjectDetails {

	private String currentProjectName;
	private String currentProjectPhase;
	private String currentProjectActivity;
	private String currentProjectModule;
	private String currentProjectLocation;
	private String currentProjectHourSpent;
	private String currentProjectMinutesSpent;
	private String currentTaskDescription;
	private String currentReferenceTicketId;
	private String timeSheetDateDifference;
	private String timeSheetDefaultHours;
	private String timeSheetMinimumHoursNeedToSpent;
	
	public String getCurrentProjectName() {
		return currentProjectName;
	}
	public void setCurrentProjectName(String currentProjectName) {
		this.currentProjectName = currentProjectName;
	}
	public String getCurrentProjectPhase() {
		return currentProjectPhase;
	}
	public void setCurrentProjectPhase(String currentProjectPhase) {
		this.currentProjectPhase = currentProjectPhase;
	}
	public String getCurrentProjectActivity() {
		return currentProjectActivity;
	}
	public void setCurrentProjectActivity(String currentProjectActivity) {
		this.currentProjectActivity = currentProjectActivity;
	}
	public String getCurrentProjectModule() {
		return currentProjectModule;
	}
	public void setCurrentProjectModule(String currentProjectModule) {
		this.currentProjectModule = currentProjectModule;
	}
	public String getCurrentProjectLocation() {
		return currentProjectLocation;
	}
	public void setCurrentProjectLocation(String currentProjectLocation) {
		this.currentProjectLocation = currentProjectLocation;
	}
	public String getCurrentProjectHourSpent() {
		return currentProjectHourSpent;
	}
	public void setCurrentProjectHourSpent(String currentProjectHourSpent) {
		this.currentProjectHourSpent = currentProjectHourSpent;
	}
	public String getCurrentProjectMinutesSpent() {
		return currentProjectMinutesSpent;
	}
	public void setCurrentProjectMinutesSpent(String currentProjectMinutesSpent) {
		this.currentProjectMinutesSpent = currentProjectMinutesSpent;
	}
	public String getCurrentTaskDescription() {
		return currentTaskDescription;
	}
	public void setCurrentTaskDescription(String currentTaskDescription) {
		this.currentTaskDescription = currentTaskDescription;
	}
	public String getCurrentReferenceTicketId() {
		return currentReferenceTicketId;
	}
	public void setCurrentReferenceTicketId(String currentReferenceTicketId) {
		this.currentReferenceTicketId = currentReferenceTicketId;
	}
	public String getTimeSheetDateDifference() {
		return timeSheetDateDifference;
	}
	public void setTimeSheetDateDifference(String timeSheetDateDifference) {
		this.timeSheetDateDifference = timeSheetDateDifference;
	}
	public String getTimeSheetDefaultHours() {
		return timeSheetDefaultHours;
	}
	public void setTimeSheetDefaultHours(String timeSheetDefaultHours) {
		this.timeSheetDefaultHours = timeSheetDefaultHours;
	}
	public String getTimeSheetMinimumHoursNeedToSpent() {
		return timeSheetMinimumHoursNeedToSpent;
	}
	public void setTimeSheetMinimumHoursNeedToSpent(String timeSheetMinimumHoursNeedToSpent) {
		this.timeSheetMinimumHoursNeedToSpent = timeSheetMinimumHoursNeedToSpent;
	}

}
