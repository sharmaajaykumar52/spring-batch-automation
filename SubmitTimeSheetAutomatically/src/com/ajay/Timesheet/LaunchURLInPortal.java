package com.ajay.Timesheet;

import java.util.concurrent.Callable;

import org.openqa.selenium.WebDriver;

public class LaunchURLInPortal implements Callable<String> {

	WebDriver driver;
	String url;

	@Override
	public String call() {
		String message = "";
		try {
			driver.get(url);
			message = "Url " + url + " Opened";
		} catch (Exception e) {
			System.out.println("Exception Occurs Inside the LaunchUrlInPortal " + e.getMessage());
			message = "";
		}
		return message;
	}

	public LaunchURLInPortal(WebDriver driver, String url) {
		super();
		this.driver = driver;
		this.url = url;
	}

}
