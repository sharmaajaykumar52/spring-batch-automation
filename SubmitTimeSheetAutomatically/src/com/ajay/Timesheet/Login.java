package com.ajay.Timesheet;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.ajay.Constants.Constants;
import com.ajay.utility.UtilityFile;

public class Login {

	public void loginIntoTimesheetModule(WebDriver driver, Map<String, Object> propertiesFileData) throws AWTException {
		String[] userNameInfo = UtilityFile
				.getStringArray((String) propertiesFileData.get(Constants.IRIS_PORTAL_USERNAME));
		String[] passwordInfo = UtilityFile
				.getStringArray((String) propertiesFileData.get(Constants.IRIS_PORTAL_PASSWORD));
		Robot robot = new Robot();
		if (UtilityFile.isCapsLockOn(robot)) {
			Toolkit.getDefaultToolkit().setLockingKeyState(KeyEvent.VK_CAPS_LOCK, false);
		}
		for (String user : userNameInfo)
			UtilityFile.typeCharacter(robot, user);
		robot.keyPress(KeyEvent.VK_TAB);
		for (String pass : passwordInfo)
			UtilityFile.typeCharacter(robot, pass);
		robot.keyPress(KeyEvent.VK_ENTER);
	}

}
