package com.ajay.mainMethod;

import java.awt.AWTException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.ajay.Cmd.Process.killProcess;
import com.ajay.Constants.Constants;
import com.ajay.POJO.ProjectDetails;
import com.ajay.Timesheet.LaunchURLInPortal;
import com.ajay.Timesheet.Login;
import com.ajay.Timesheet.TimeTrackingSystem;
import com.ajay.propertiesFile.Read;
import com.ajay.utility.UtilityFile;

public class MainMethod {

	static Map<String, Object> propertiesFileData = null;

	static {
		propertiesFileData = Read.readPropertyFile(Constants.POROPERTIES_FILE_LOC);
		System.setProperty("webdriver.chrome.driver", (String) propertiesFileData.get(Constants.CHROME_DRIVER_LOC));
		System.setProperty("webdriver.gecko.driver",
				(String) propertiesFileData.get(Constants.FIREFOX_DRIVER_LOCATION));
	}

	public static void main(String[] args) throws AWTException, MalformedURLException, InterruptedException {
		ExecutorService executor = Executors.newFixedThreadPool(2);
		try {
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@");
			System.out.println("@@@@@   Timesheet Filling Batch : Start @@@@@");
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@");
			propertiesFileData.entrySet().stream().forEach(x -> System.out.println(x));
			WebDriver driver = new FirefoxDriver();

			// Launching the IRIS Portal
			LaunchURLInPortal launchIRISPortalObj = new LaunchURLInPortal(driver,
					(String) propertiesFileData.get(Constants.IRIS_PORTAL_URL));
			executor.submit(launchIRISPortalObj);

			// Login into Portal
			Login login = new Login();
			login.loginIntoTimesheetModule(driver, propertiesFileData);
			System.out.println("Login Successfully");
			Thread.sleep(5000);
			TimeTrackingSystem timeTrackingSystemObj = new TimeTrackingSystem();
			timeTrackingSystemObj.open(driver, propertiesFileData);

			List<ProjectDetails> projectDetailsList = UtilityFile.getProjectDetails(propertiesFileData);			
			
			
			timeTrackingSystemObj.fillTimeSheet(driver, projectDetailsList);

			System.out.println("User Logged In Successfully ");
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@");
			System.out.println("@@@@@   Timesheet Filling Batch : End @@@@@");
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@");
			executor.shutdown();
			killProcess killProcessObj = new killProcess();
			killProcessObj.killCmdProcess("chromedriver.exe");
		} finally {
			/*
			 * executor.shutdown(); killProcess killProcessObj = new killProcess();
			 * killProcessObj.killCmdProcess("chromedriver.exe"); System.exit(0);
			 */

		}
	}

	// *[@id="divMainMenu"]/ul/li[3]/ul/li[2]/a

}
