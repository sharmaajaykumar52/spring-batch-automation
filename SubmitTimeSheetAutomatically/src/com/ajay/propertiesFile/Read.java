package com.ajay.propertiesFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

public class Read {

	public static Map<String, Object> readPropertyFile(String propertyFileLoication) {
		Map<String, Object> propertiesFileData = null;
		try (InputStream inputStream = new FileInputStream(new File(propertyFileLoication));) {
			Properties propertiesObj = new Properties();
			propertiesObj.load(inputStream);
			propertiesFileData = propertiesObj.stringPropertyNames().stream()
					.collect(Collectors.toMap(x -> x, x -> propertiesObj.get(x)));
		} catch (IOException ex) {
			System.out.println("Checked Exception Occurs " + ex.getMessage());
		}
		return propertiesFileData;
	}

}
