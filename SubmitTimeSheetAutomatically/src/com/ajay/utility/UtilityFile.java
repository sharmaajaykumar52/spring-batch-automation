package com.ajay.utility;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;

import com.ajay.Constants.Constants;
import com.ajay.POJO.ProjectDetails;

public class UtilityFile {

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
	private static final long MILLIS_PER_DAY = 24 * 60 * 60 * 1000;

	public static String toHex(String arg) throws UnsupportedEncodingException {
		String outPut = String.format("%040x", new BigInteger(1, arg.getBytes("UTF-8"))).trim();
		System.out.println("outPut " + outPut + "toAsciiValue " + toAsciiValue(arg));
		System.out.println(" Integer Value Is " + Integer.parseInt(outPut));
		return outPut;
	}

	public static String toAsciiValue(String hexadecimalValue) {
		StringBuilder output = new StringBuilder("");
		for (int i = 0; i < hexadecimalValue.length(); i += 2) {
			String str = hexadecimalValue.substring(i, i + 2);
			output.append((char) Integer.parseInt(str, 16));
		}
		return hexadecimalValue;
	}

	public static void typeCharacter(Robot robot, String letter) {
		try {

			boolean upperCase = Character.isUpperCase(letter.charAt(0));
			if (String.valueOf(letter.charAt(0)).equals(".")) {
				letter = "PERIOD";
			}
			if (String.valueOf(letter.charAt(0)).equals("@")) {
				letter = "2";
				upperCase = true;
			}
			if (String.valueOf(letter.charAt(0)).equals("/")) {
				letter = "DIVIDE";
			}
			String variableName = "VK_" + letter.toUpperCase().trim();
			Class<?> clazz = KeyEvent.class;
			Field field = clazz.getField(variableName);
			int keyCode = field.getInt(null);
			robot.delay(1000);
			if (upperCase)
				robot.keyPress(KeyEvent.VK_SHIFT);
			robot.keyPress(keyCode);
			robot.keyRelease(keyCode);
			if (upperCase)
				robot.keyRelease(KeyEvent.VK_SHIFT);
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public static String[] getStringArray(String inputString) {
		return inputString.split("(?!^)");
	}

	public static boolean isAlertPresent(WebDriver ldriver) {
		try {
			ldriver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException ex) {
			return false;
		}
	}

	public static void handleAlert(WebDriver ldriver) {
		if (isAlertPresent(ldriver)) {
			Alert alert = ldriver.switchTo().alert();
			System.out.println(alert.getText());
			alert.accept();
		}
	}

	public static boolean isDateEqual(String dateObj, Date todaysDate) {
		return dateObj.equals(dateFormat.format(todaysDate));
	}

	public static Date addDays(Date d, int numDays) {
		return new Date(d.getTime() + numDays * MILLIS_PER_DAY);
	}

	// it will accept the hours and minutes in array
	public static String getTotalTime(String[] hours, String[] minutes) {
		int totalHours = 0;
		int totalminutes = 0;
		totalHours = calculateTotalHours(hours);
		totalminutes = calculateTotalMinutes(minutes);
		while (totalminutes > 60) {
			totalminutes = totalminutes - 60;
			totalHours = totalHours + 1;
		}
		return String.valueOf(totalHours) + ":" + String.valueOf(totalminutes);
	}

	public static String getTotalTime(List<String> hours, List<String> minutes) {
		int totalHours = 0;
		int totalminutes = 0;
		totalHours = calculateTotalHours(hours.stream().toArray(String[]::new));
		totalminutes = calculateTotalMinutes(minutes.stream().toArray(String[]::new));
		while (totalminutes >= 60) {
			totalminutes = totalminutes - 60;
			totalHours = totalHours + 1;
		}
		return String.valueOf(totalHours) + ":" + String.valueOf(totalminutes);
	}

	private static int calculateTotalHours(String[] hours) {
		int totalHours = 0;
		for (int i = 0; i < hours.length; i++) {
			int totalHrs = Integer.parseInt(hours[i]);
			totalHours += totalHrs;
		}
		return totalHours;
	}

	private static int calculateTotalMinutes(String[] minutes) {
		int totalMinutes = 0;
		for (String min : minutes) {
			totalMinutes += Integer.parseInt((String) min);
		}
		return totalMinutes;
	}

	public static boolean isCapsLockOn(Robot robot) throws AWTException {
		return Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);
	}

	public static String getDateInString(Date date) {
		return dateFormat.format(date);

	}

	public static List<ProjectDetails> getProjectDetails(Map<String, Object> propertiesDetails) {
		List<ProjectDetails> projectDetailsList = new ArrayList<ProjectDetails>();
		ProjectDetails projectDetails = null;
		String[] currentProjectName = ((String) propertiesDetails.get(Constants.CURRENT_PROJECT_NAME))
				.split(Pattern.quote(Constants.DELIMINATOR));
		String[] currentPhase = ((String) propertiesDetails.get(Constants.CURRENT_PHASE))
				.split(Pattern.quote(Constants.DELIMINATOR));
		String[] currentActivity = ((String) propertiesDetails.get(Constants.CURRENT_ACTIVITY))
				.split(Pattern.quote(Constants.DELIMINATOR));
		String[] currentModule = ((String) propertiesDetails.get(Constants.CURRENT_MODULE))
				.split(Pattern.quote(Constants.DELIMINATOR));
		String[] currentLocation = ((String) propertiesDetails.get(Constants.CURRENT_LOCATION))
				.split(Pattern.quote(Constants.DELIMINATOR));
		String[] hoursSpent = ((String) propertiesDetails.get(Constants.HOURS_SPENT))
				.split(Pattern.quote(Constants.DELIMINATOR));
		String[] minutesSpent = ((String) propertiesDetails.get(Constants.MINUTES_SPENT))
				.split(Pattern.quote(Constants.DELIMINATOR));
		String[] currentProjectDescription = ((String) propertiesDetails.get(Constants.CURRENT_PROJECT_DESCRIPTION))
				.split(Pattern.quote(Constants.DELIMINATOR));
		String[] referenceTicketId = ((String) propertiesDetails.get(Constants.REFERENCE_TICKET_ID))
				.split(Pattern.quote(Constants.DELIMINATOR));
		String dateDifference = (String) propertiesDetails.get(Constants.TIME_SHEET_DATE_DIFFERENCE);
		String timeSheetDefaultHours = (String) propertiesDetails.get(Constants.DEFAULT_HOURS);
		String timeSheetMinimumHours=(String)propertiesDetails.get(Constants.TIME_SHEET_MINIMUM_HOURS);
		boolean isProjectDataValid = checkSizeOfArray(currentProjectName, currentPhase, currentActivity, currentModule,
				currentLocation, hoursSpent, minutesSpent, currentProjectDescription, referenceTicketId);
		if (isProjectDataValid) {
			int arraySize = currentProjectName.length;
			System.out.println(" arraySize    " + arraySize);
			for (int i = 0; i < arraySize; i++) {
				projectDetails = new ProjectDetails();
				projectDetails.setCurrentProjectName(currentProjectName[i]);
				projectDetails.setCurrentProjectActivity(currentActivity[i]);
				projectDetails.setCurrentProjectHourSpent(hoursSpent[i]);
				projectDetails.setCurrentProjectLocation(currentLocation[i]);
				projectDetails.setCurrentProjectMinutesSpent(minutesSpent[i]);
				projectDetails.setCurrentProjectModule(currentModule[i]);
				projectDetails.setCurrentProjectPhase(currentPhase[i]);
				projectDetails.setCurrentReferenceTicketId(referenceTicketId[i]);
				projectDetails.setCurrentTaskDescription(currentProjectDescription[i]);
				projectDetailsList.add(projectDetails);
				projectDetails.setTimeSheetDefaultHours(timeSheetDefaultHours);
				projectDetails.setTimeSheetDateDifference(dateDifference);
				projectDetails.setTimeSheetMinimumHoursNeedToSpent(timeSheetMinimumHours);
			}
		}
		return projectDetailsList;
	}

	private static boolean checkSizeOfArray(String[]... inputStringArray) {
		boolean isProjectDataValid = false;
		int arraySize = inputStringArray[0].length;
		for (String[] inputString : inputStringArray) {
			if (!inputString.equals(null)) {
				if (arraySize == inputString.length) {
					isProjectDataValid = true;
					continue;
				} else {
					isProjectDataValid = false;
				}
			} else {
				isProjectDataValid = false;
				break;
			}
		}
		return isProjectDataValid;
	}

}
