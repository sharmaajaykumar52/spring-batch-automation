package com.ajay.submitTimeSheetBatch.Constants;

public interface Constants {

	public static final String POROPERTIES_FILE_LOC = "E:\\Study WorkSpace\\Automate Basic Task\\Timesheet\\PropertiesFile\\TimesheetPropertiesFile.properties";
	public static final String CHROME_DRIVER_LOC = "CHROME_DRIVER_LOCATION";
	public static final String FIREFOX_DRIVER_LOCATION = "FIREFOX_DRIVER_LOCATION";
	public static final String IRIS_PORTAL_USERNAME = "USERNAME";
	public static final String IRIS_PORTAL_PASSWORD = "PASSWORD";
	public static final String IRIS_PORTAL_URL = "URL";
	public static final String HEADER_TAG_SELECTION = "HEADER_TAG_SELECTION";
	public static final String TAG_SELECTION = "TAG_SELECTION";
	public static final String DEFAULT_HOURS = "0";
	public static final String DEFAULT_MINUTES = "0";
	public static final String TIME_SHEET_DATE_DIFFERENCE = "TIME_SHEET_DATE_DIFFERENCE";
	public static final String TIME_SHEET_MINIMUM_HOURS = "TIME_SHEET_MINIMUM_HOURS";
	public static final String CURRENT_PROJECT_NAME = "CURRENT_PROJECT_NAME";
	public static final String CURRENT_PHASE = "CURRENT_PHASE";
	public static final String CURRENT_ACTIVITY = "CURRENT_ACTIVITY";
	public static final String CURRENT_MODULE = "CURRENT_MODULE";
	public static final String CURRENT_LOCATION = "CURRENT_LOCATION";
	public static final String HOURS_SPENT = "HOURS_SPENT";
	public static final String MINUTES_SPENT = "MINUTES_SPENT";
	public static final String CURRENT_PROJECT_DESCRIPTION = "CURRENT_PROJECT_DESCRIPTION";
	public static final String REFERENCE_TICKET_ID = "REFERENCE_TICKET_ID";
	public static final String DELIMINATOR="@#$";
}
