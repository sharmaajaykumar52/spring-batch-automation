package com.ajay.submitTimeSheetBatch.Controller;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/submit")
public class SubmitTimeSheet {

	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	Job processJob;

	@GetMapping(value = "/timesheet")
	public String submitTimeSheetOfEmployee() throws JobExecutionAlreadyRunningException, JobRestartException,
			JobInstanceAlreadyCompleteException, JobParametersInvalidException {
		System.setProperty("java.awt.headless", "false");
		JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
				.toJobParameters();
		jobLauncher.run(processJob, jobParameters);
		return "Batch job has been invoked";
	}

	@Autowired
	private JavaMailSender sender;

	@GetMapping(value = "/sendEmail")
	public String sendEmailNotification() {
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		try {
			message.setFrom(new InternetAddress("healthcare@infogain.com"));
			System.out.println("Trying  to Send Email Notification........");
			helper.setTo("ajay.sharma@irissoftware.com");
			helper.setText("Greetings :)");
			helper.setSubject("Mail From Spring Batch Configuration Test");
			System.out.println("Email Send successfully ..........");
		} catch (MessagingException e) {
			return "Error while sending mail " + e.getMessage() + ".....";
		}
		sender.send(message);
		return "Email Send Successfully";
	}

}
