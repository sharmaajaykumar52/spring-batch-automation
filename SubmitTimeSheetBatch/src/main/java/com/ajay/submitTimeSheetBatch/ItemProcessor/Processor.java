package com.ajay.submitTimeSheetBatch.ItemProcessor;

import java.util.List;
import java.util.Map;

import org.springframework.batch.item.ItemProcessor;

import com.ajay.submitTimeSheetBatch.Utility.UtilityFile;
import com.ajay.submitTimeSheetBatch.pojo.ProjectDetails;

public class Processor implements ItemProcessor<Map<String, Object>, List<ProjectDetails>> {

	@Override
	public List<ProjectDetails> process(Map<String, Object> propertiesFileData) throws Exception {
		propertiesFileData.entrySet().stream().forEach(propertiesFileObj -> System.out.println(propertiesFileObj));
		List<ProjectDetails> projectDetailsListObj = UtilityFile.getProjectDetails(propertiesFileData);
		return projectDetailsListObj;
	}

}
