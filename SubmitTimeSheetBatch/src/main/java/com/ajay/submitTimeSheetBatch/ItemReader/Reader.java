package com.ajay.submitTimeSheetBatch.ItemReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.springframework.batch.item.ItemReader;
import org.springframework.util.ResourceUtils;

public class Reader implements ItemReader<Map<String, Object>> {

	boolean dataSentToProcessor = false;

	@Override
	public Map<String, Object> read() {
		if (!dataSentToProcessor) {
			Properties propertiesObj = new Properties();
			File file;
			try {
				file = ResourceUtils.getFile("classpath:TimesheetPropertiesFile.properties");
				InputStream in = new FileInputStream(file);
				propertiesObj.load(in);
				Map<String, Object> propertiesFileData = propertiesObj.stringPropertyNames().stream()
						.collect(Collectors.toMap(x -> x, x -> propertiesObj.get(x)));
				dataSentToProcessor = true;
				return propertiesFileData;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			return null;
		}
		return null;

	}
}
