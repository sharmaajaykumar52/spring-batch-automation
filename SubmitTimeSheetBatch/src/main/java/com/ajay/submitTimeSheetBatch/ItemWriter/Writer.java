package com.ajay.submitTimeSheetBatch.ItemWriter;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.ajay.submitTimeSheetBatch.killProcess;
import com.ajay.submitTimeSheetBatch.TimeSheet.LaunchURLInPortal;
import com.ajay.submitTimeSheetBatch.TimeSheet.Login;
import com.ajay.submitTimeSheetBatch.TimeSheet.TimeTrackingSystem;
import com.ajay.submitTimeSheetBatch.pojo.ProjectDetails;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Writer implements ItemWriter<List<ProjectDetails>> {

	@Autowired
	killProcess killProcessObj;

	@Override
	public void write(List<? extends List<ProjectDetails>> projectDetailsList) throws Exception {
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@");
		System.out.println("@@@@@   Timesheet Filling Batch : Start @@@@@");
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@");

		List<ProjectDetails> projectDetailsListObj = projectDetailsList.stream()
				.flatMap(projectDetails -> projectDetails.stream()).distinct().collect(Collectors.toList());
		ExecutorService executorServiceObj =  Executors.newFixedThreadPool(2);
		WebDriverManager.firefoxdriver().setup();		
		WebDriver driver = new FirefoxDriver();
		LaunchURLInPortal launchIRISPortalObj = new LaunchURLInPortal(driver,
				projectDetailsListObj.get(0).getIrisPortalUrl());
		executorServiceObj.submit(launchIRISPortalObj);

		// Login into Portal
		Login login = new Login();
		login.loginIntoTimesheetModule(driver, projectDetailsListObj.get(0));
		System.out.println("Login Successfully");
		Thread.sleep(5000);
		TimeTrackingSystem timeTrackingSystemObj = new TimeTrackingSystem();
		timeTrackingSystemObj.open(driver, projectDetailsListObj.get(0));
		timeTrackingSystemObj.fillTimeSheet(driver, projectDetailsListObj);

		System.out.println("User Logged In Successfully ");
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@");
		System.out.println("@@@@@   Timesheet Filling Batch : End @@@@@");
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@");
		//killProcessObj.killCmdProcess("chromedriver.exe");

	}
}
