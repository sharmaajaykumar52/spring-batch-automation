package com.ajay.submitTimeSheetBatch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableBatchProcessing
@EnableScheduling
public class SubmitTimeSheetBatchApplication {

	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	Job processJob;

	public static void main(String[] args) {
		SpringApplication.run(SubmitTimeSheetBatchApplication.class, args);
	}

	//@Scheduled(cron = "0 */1 * * * ?")
	public void StartBatchJob() throws Exception {
		System.setProperty("java.awt.headless", "false");
		JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
				.toJobParameters();
		jobLauncher.run(processJob, jobParameters);

	}

}
