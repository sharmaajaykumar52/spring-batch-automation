package com.ajay.submitTimeSheetBatch.TimeSheet;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebDriver;

import com.ajay.submitTimeSheetBatch.Utility.UtilityFile;
import com.ajay.submitTimeSheetBatch.pojo.ProjectDetails;



public class Login {

	public void loginIntoTimesheetModule(WebDriver driver,ProjectDetails projectDetailsListObj ) throws AWTException {
		String[] userNameInfo = UtilityFile
				.getStringArray(projectDetailsListObj.getUserName());
		String[] passwordInfo = UtilityFile
				.getStringArray(projectDetailsListObj.getPassword());
		Robot robot = new Robot();
		if (UtilityFile.isCapsLockOn(robot)) {
			Toolkit.getDefaultToolkit().setLockingKeyState(KeyEvent.VK_CAPS_LOCK, false);
		}
		for (String user : userNameInfo)
			UtilityFile.typeCharacter(robot, user);
		robot.keyPress(KeyEvent.VK_TAB);
		for (String pass : passwordInfo)
			UtilityFile.typeCharacter(robot, pass);
		robot.keyPress(KeyEvent.VK_ENTER);
	}

}
