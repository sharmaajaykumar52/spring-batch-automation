package com.ajay.submitTimeSheetBatch.TimeSheet;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.ajay.submitTimeSheetBatch.Constants.Constants;
import com.ajay.submitTimeSheetBatch.Utility.UtilityFile;
import com.ajay.submitTimeSheetBatch.pojo.ProjectDetails;


public class TimeTrackingSystem {

	public void open(WebDriver driver, ProjectDetails projectDetailsObj) throws InterruptedException {
		Thread.sleep(10000);
		WebElement element = driver
				.findElement(By.linkText(projectDetailsObj.getHeaderTagSelection()));
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
		driver.findElement(By.linkText(projectDetailsObj.getTagSelection())).click();
	}

	public void fillTimeSheet(WebDriver driver, List<? extends ProjectDetails> projectDetailsList)
			throws InterruptedException, AWTException {

		System.out.println("TimeSheet Filling Start");
		Thread.sleep(10000);

		List<WebElement> parentElementList = driver
				.findElements(By.xpath("/html/body/div[2]/div[1]/div/div/div/form/div/div[5]/div/div/div[2]/div"));

		List<String> hoursList = new ArrayList<String>();
		List<String> minutesList = new ArrayList<>();
		String dateFromTimeSheet = "";

		Date dateObj = UtilityFile.addDays(new Date(),
				Integer.parseInt(projectDetailsList.get(0).getTimeSheetDateDifference()));
		hoursList.add(Constants.DEFAULT_HOURS);
		minutesList.add(Constants.DEFAULT_MINUTES);
		String totalTime = calculateTime(driver, dateFromTimeSheet, parentElementList, dateObj, hoursList, minutesList);

		if (Integer.parseInt(totalTime.split(":")[0]) >= Integer
				.parseInt(projectDetailsList.get(0).getTimeSheetMinimumHoursNeedToSpent())) {
			System.out.println("No Need to Fill the timeSheet as its already Filled");
		} else if (Integer.parseInt(totalTime.split(":")[0]) < 8 && Integer.parseInt(totalTime.split(":")[0]) > 0) {
			System.out.println("Please Fill the timeSheet for day " + dateFromTimeSheet + " as its [artiall filled");
		} else if (Integer.parseInt(totalTime.split(":")[0]) == 0) {
			System.out.println("TimeSheet Missing for the day " + dateObj);

			Select projectList = new Select(driver.findElement(By.xpath("//*[@id=\"ProjectList_AddRow\"]")));
			Select phaseList = new Select(driver.findElement(By.xpath("//*[@id=\"PhaseList_AddRow\"]")));
			Select activityList = new Select(driver.findElement(By.xpath("//*[@id=\"ActivityList_AddRow\"]")));
			Select moduleList = new Select(driver.findElement(By.xpath("//*[@id=\"ModuleList_AddRow\"]")));
			Select locationList = new Select(driver.findElement(By.xpath("//*[@id=\"LocationList_AddRow\"]")));

			for (int i = 0; i < projectDetailsList.size(); i++) {

				driver.findElement(By.xpath("//*[@id=\"TTSDate_AddRow\"]")).clear();
				driver.findElement(By.xpath("//*[@id=\"TTSDate_AddRow\"]"))
						.sendKeys(UtilityFile.getDateInString(dateObj));

				Robot robot = new Robot();
				robot.keyPress(KeyEvent.VK_TAB);
				implicitWait(4000);

				List<WebElement> projectListValue = projectList.getOptions();
				for (WebElement projectElement : projectListValue) {
					String projectName = projectDetailsList.get(i).getCurrentProjectName();
					if (projectName.equals(projectElement.getAttribute("label"))) {
						projectList.selectByVisibleText(projectName);
					}
				}
				implicitWait(4000);
				List<WebElement> phaseListValue = phaseList.getOptions();
				for (WebElement phaseListElement : phaseListValue) {
					String phase = projectDetailsList.get(i).getCurrentProjectPhase();
					if (phase.equals(phaseListElement.getAttribute("label"))) {
						phaseList.selectByVisibleText(phase);
					}
				}
				implicitWait(4000);
				List<WebElement> activityListValue = activityList.getOptions();
				for (WebElement activityElement : activityListValue) {
					String activity = projectDetailsList.get(i).getCurrentProjectActivity();
					if (activity.equals(activityElement.getAttribute("label"))) {
						activityList.selectByVisibleText(activity);
					}
				}
				implicitWait(4000);
				List<WebElement> moduleListValue = moduleList.getOptions();
				for (WebElement moduleElement : moduleListValue) {
					String module = projectDetailsList.get(i).getCurrentProjectModule();
					if (module.equals(moduleElement.getAttribute("label"))) {
						moduleList.selectByVisibleText(module);
					}
				}
				implicitWait(4000);
				List<WebElement> locationListValue = locationList.getOptions();
				for (WebElement locationElement : locationListValue) {
					String location = projectDetailsList.get(i).getCurrentProjectLocation();
					if (location.equals(locationElement.getAttribute("label"))) {
						locationList.selectByVisibleText(locationElement.getAttribute("label"));
					}

				}
				implicitWait(1000);
				driver.findElement(By.xpath("//*[@id=\"Hrs_AddRow\"]")).clear();
				driver.findElement(By.xpath("//*[@id=\"Hrs_AddRow\"]"))
						.sendKeys(projectDetailsList.get(i).getCurrentProjectHourSpent());
				implicitWait(1000);
				driver.findElement(By.xpath("//*[@id=\"Mins_AddRow\"]")).clear();
				;
				driver.findElement(By.xpath("//*[@id=\"Mins_AddRow\"]"))
						.sendKeys(projectDetailsList.get(i).getCurrentProjectMinutesSpent());
				implicitWait(1000);
				driver.findElement(By.xpath("//*[@id=\"Description_AddRow\"]"))
						.sendKeys(projectDetailsList.get(i).getCurrentTaskDescription());
				implicitWait(1000);
				driver.findElement(By.xpath("//*[@id=\"TicketId_AddRow\"]"))
						.sendKeys(projectDetailsList.get(i).getCurrentReferenceTicketId());
				implicitWait(1000);
				driver.findElement(By.xpath("//*[@id=\"btnAddRow\"]")).click();
			}
			
			
			implicitWait(4000);			
			try {
				driver.findElement(By.xpath("//input[type='button']")).click();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(
						"			driver.findElement(By.xpath(\"//*[@type='button']\")).click();; 			driver.findElement(By.xpath(\"//*[@type='button']\")).click();;");
				driver.findElement(By.xpath("//*[@type='button']")).click();
				;
			}
			/*
			 * driver.findElement(By.
			 * cssSelector("div.form-group:nth-child(10) > div:nth-child(6) > button:nth-child(2)"
			 * )) .click();
			 */
				 
			implicitWait(2000);
		}

		List<WebElement> newParentElementList = driver
				.findElements(By.xpath("/html/body/div[2]/div[1]/div/div/div/form/div/div[5]/div/div/div[2]/div"));
		String finalTotalTime = calculateTime(driver, dateFromTimeSheet, newParentElementList, dateObj, hoursList,
				minutesList);
		System.out.println("finalTotalTime finalTotalTime finalTotalTime finalTotalTime " + finalTotalTime);
		if (Integer.parseInt(finalTotalTime.split(":")[0]) >= Integer
				.parseInt(projectDetailsList.get(0).getTimeSheetMinimumHoursNeedToSpent())) {
			System.out.println("Time sheet filled for the date " + dateFromTimeSheet + " successfully");
		} else if (Integer.parseInt(finalTotalTime.split(":")[0]) < 8
				&& Integer.parseInt(finalTotalTime.split(":")[0]) > 0) {
			System.out.println("Time sheet filled partially successfully for date " + dateFromTimeSheet);
		}
	}

	public void submitTimeSheet(WebDriver driver) {
		System.out.println("TimeSheet Submitted !!!!");
		driver.findElement(By.xpath("//*[@id=\"frmTTS\"]/div/div[5]/div/div/div[2]/div[8]/div[6]/button[2]")).click();
	}

	public static void implicitWait(long time) throws InterruptedException {
		Thread.sleep(time);
	}

	private static String calculateTime(WebDriver driver, String dateFromTimeSheet, List<WebElement> parentElementList,
			Date dateObj, List<String> hoursList, List<String> minutesList) {
		try {
			for (int i = 3; i < parentElementList.size() - 1; i++) {
				dateFromTimeSheet = driver.findElement(By.xpath("//*[@id=\"TTSDate_" + (i - 3) + "\"]"))
						.getAttribute("value");
				if (UtilityFile.isDateEqual(dateFromTimeSheet, dateObj)) {
					System.out.println("Valid Date Found ");
					String hoursFromTimeSheet = driver.findElement(By.xpath("//*[@id=\"Hrs_" + (i - 3) + "\"]"))
							.getAttribute("value");
					String minutesFromTimeSheet = driver.findElement(By.xpath("//*[@id=\"Mins_" + (i - 3) + "\"]"))
							.getAttribute("value");
					hoursList.add(hoursFromTimeSheet);
					minutesList.add(minutesFromTimeSheet);
				}
			}
		} catch (Exception e) {
			System.out.println("TimeSheet Missing for the day " + dateObj);
		}
		String totalTime = UtilityFile.getTotalTime(hoursList, minutesList);
		return totalTime;
	}

}
