package com.ajay.submitTimeSheetBatch.pojo;

public class ProjectDetails {

	private String currentProjectName;
	private String currentProjectPhase;
	private String currentProjectActivity;
	private String currentProjectModule;
	private String currentProjectLocation;
	private String currentProjectHourSpent;
	private String currentProjectMinutesSpent;
	private String currentTaskDescription;
	private String currentReferenceTicketId;
	private String timeSheetDateDifference;
	private String timeSheetDefaultHours;
	private String timeSheetMinimumHoursNeedToSpent;
	private String irisPortalUrl;
	private String userName;
	private String password;
	private String headerTagSelection;
	private String tagSelection;

	public String getCurrentProjectName() {
		return currentProjectName;
	}

	public void setCurrentProjectName(String currentProjectName) {
		this.currentProjectName = currentProjectName;
	}

	public String getCurrentProjectPhase() {
		return currentProjectPhase;
	}

	public void setCurrentProjectPhase(String currentProjectPhase) {
		this.currentProjectPhase = currentProjectPhase;
	}

	public String getCurrentProjectActivity() {
		return currentProjectActivity;
	}

	public void setCurrentProjectActivity(String currentProjectActivity) {
		this.currentProjectActivity = currentProjectActivity;
	}

	public String getCurrentProjectModule() {
		return currentProjectModule;
	}

	public void setCurrentProjectModule(String currentProjectModule) {
		this.currentProjectModule = currentProjectModule;
	}

	public String getCurrentProjectLocation() {
		return currentProjectLocation;
	}

	public void setCurrentProjectLocation(String currentProjectLocation) {
		this.currentProjectLocation = currentProjectLocation;
	}

	public String getCurrentProjectHourSpent() {
		return currentProjectHourSpent;
	}

	public void setCurrentProjectHourSpent(String currentProjectHourSpent) {
		this.currentProjectHourSpent = currentProjectHourSpent;
	}

	public String getCurrentProjectMinutesSpent() {
		return currentProjectMinutesSpent;
	}

	public void setCurrentProjectMinutesSpent(String currentProjectMinutesSpent) {
		this.currentProjectMinutesSpent = currentProjectMinutesSpent;
	}

	public String getCurrentTaskDescription() {
		return currentTaskDescription;
	}

	public void setCurrentTaskDescription(String currentTaskDescription) {
		this.currentTaskDescription = currentTaskDescription;
	}

	public String getCurrentReferenceTicketId() {
		return currentReferenceTicketId;
	}

	public void setCurrentReferenceTicketId(String currentReferenceTicketId) {
		this.currentReferenceTicketId = currentReferenceTicketId;
	}

	public String getTimeSheetDateDifference() {
		return timeSheetDateDifference;
	}

	public void setTimeSheetDateDifference(String timeSheetDateDifference) {
		this.timeSheetDateDifference = timeSheetDateDifference;
	}

	public String getTimeSheetDefaultHours() {
		return timeSheetDefaultHours;
	}

	public void setTimeSheetDefaultHours(String timeSheetDefaultHours) {
		this.timeSheetDefaultHours = timeSheetDefaultHours;
	}

	public String getTimeSheetMinimumHoursNeedToSpent() {
		return timeSheetMinimumHoursNeedToSpent;
	}

	public void setTimeSheetMinimumHoursNeedToSpent(String timeSheetMinimumHoursNeedToSpent) {
		this.timeSheetMinimumHoursNeedToSpent = timeSheetMinimumHoursNeedToSpent;
	}

	public String getIrisPortalUrl() {
		return irisPortalUrl;
	}

	public void setIrisPortalUrl(String irisPortalUrl) {
		this.irisPortalUrl = irisPortalUrl;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHeaderTagSelection() {
		return headerTagSelection;
	}

	public void setHeaderTagSelection(String headerTagSelection) {
		this.headerTagSelection = headerTagSelection;
	}

	public String getTagSelection() {
		return tagSelection;
	}

	public void setTagSelection(String tagSelection) {
		this.tagSelection = tagSelection;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currentProjectActivity == null) ? 0 : currentProjectActivity.hashCode());
		result = prime * result + ((currentProjectHourSpent == null) ? 0 : currentProjectHourSpent.hashCode());
		result = prime * result + ((currentProjectLocation == null) ? 0 : currentProjectLocation.hashCode());
		result = prime * result + ((currentProjectMinutesSpent == null) ? 0 : currentProjectMinutesSpent.hashCode());
		result = prime * result + ((currentProjectModule == null) ? 0 : currentProjectModule.hashCode());
		result = prime * result + ((currentProjectName == null) ? 0 : currentProjectName.hashCode());
		result = prime * result + ((currentProjectPhase == null) ? 0 : currentProjectPhase.hashCode());
		result = prime * result + ((currentReferenceTicketId == null) ? 0 : currentReferenceTicketId.hashCode());
		result = prime * result + ((currentTaskDescription == null) ? 0 : currentTaskDescription.hashCode());
		result = prime * result + ((headerTagSelection == null) ? 0 : headerTagSelection.hashCode());
		result = prime * result + ((irisPortalUrl == null) ? 0 : irisPortalUrl.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((tagSelection == null) ? 0 : tagSelection.hashCode());
		result = prime * result + ((timeSheetDateDifference == null) ? 0 : timeSheetDateDifference.hashCode());
		result = prime * result + ((timeSheetDefaultHours == null) ? 0 : timeSheetDefaultHours.hashCode());
		result = prime * result
				+ ((timeSheetMinimumHoursNeedToSpent == null) ? 0 : timeSheetMinimumHoursNeedToSpent.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjectDetails other = (ProjectDetails) obj;
		if (currentProjectActivity == null) {
			if (other.currentProjectActivity != null)
				return false;
		} else if (!currentProjectActivity.equals(other.currentProjectActivity))
			return false;
		if (currentProjectHourSpent == null) {
			if (other.currentProjectHourSpent != null)
				return false;
		} else if (!currentProjectHourSpent.equals(other.currentProjectHourSpent))
			return false;
		if (currentProjectLocation == null) {
			if (other.currentProjectLocation != null)
				return false;
		} else if (!currentProjectLocation.equals(other.currentProjectLocation))
			return false;
		if (currentProjectMinutesSpent == null) {
			if (other.currentProjectMinutesSpent != null)
				return false;
		} else if (!currentProjectMinutesSpent.equals(other.currentProjectMinutesSpent))
			return false;
		if (currentProjectModule == null) {
			if (other.currentProjectModule != null)
				return false;
		} else if (!currentProjectModule.equals(other.currentProjectModule))
			return false;
		if (currentProjectName == null) {
			if (other.currentProjectName != null)
				return false;
		} else if (!currentProjectName.equals(other.currentProjectName))
			return false;
		if (currentProjectPhase == null) {
			if (other.currentProjectPhase != null)
				return false;
		} else if (!currentProjectPhase.equals(other.currentProjectPhase))
			return false;
		if (currentReferenceTicketId == null) {
			if (other.currentReferenceTicketId != null)
				return false;
		} else if (!currentReferenceTicketId.equals(other.currentReferenceTicketId))
			return false;
		if (currentTaskDescription == null) {
			if (other.currentTaskDescription != null)
				return false;
		} else if (!currentTaskDescription.equals(other.currentTaskDescription))
			return false;
		if (headerTagSelection == null) {
			if (other.headerTagSelection != null)
				return false;
		} else if (!headerTagSelection.equals(other.headerTagSelection))
			return false;
		if (irisPortalUrl == null) {
			if (other.irisPortalUrl != null)
				return false;
		} else if (!irisPortalUrl.equals(other.irisPortalUrl))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (tagSelection == null) {
			if (other.tagSelection != null)
				return false;
		} else if (!tagSelection.equals(other.tagSelection))
			return false;
		if (timeSheetDateDifference == null) {
			if (other.timeSheetDateDifference != null)
				return false;
		} else if (!timeSheetDateDifference.equals(other.timeSheetDateDifference))
			return false;
		if (timeSheetDefaultHours == null) {
			if (other.timeSheetDefaultHours != null)
				return false;
		} else if (!timeSheetDefaultHours.equals(other.timeSheetDefaultHours))
			return false;
		if (timeSheetMinimumHoursNeedToSpent == null) {
			if (other.timeSheetMinimumHoursNeedToSpent != null)
				return false;
		} else if (!timeSheetMinimumHoursNeedToSpent.equals(other.timeSheetMinimumHoursNeedToSpent))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

}
