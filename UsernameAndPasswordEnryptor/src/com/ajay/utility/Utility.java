package com.ajay.utility;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;

public class Utility {

	public static String getReverseString(String userInput) {
		if (userInput.length() <= 1) {
			return userInput;
		}
		return getReverseString(userInput.substring(1)) + userInput.charAt(0);
	}

	public static String getEncodedInput(String userInput) throws UnsupportedEncodingException {
		return URLEncoder.encode(userInput, "UTF-8");
	}

	public static String getHexaDecimalValue(String userInput) throws UnsupportedEncodingException {
		String outPut = String.format("%040x", new BigInteger(1, userInput.getBytes("UTF-8"))).trim();
		System.out.println("outPut " + outPut + "toAsciiValue " + toAsciiValue(userInput));
		System.out.println(" Integer Value Is " + Integer.parseInt(outPut));
		return outPut;
	}

	public static String toAsciiValue(String hexadecimalValue) {
		StringBuilder output = new StringBuilder("");
		for (int i = 0; i < hexadecimalValue.length(); i += 2) {
			String str = hexadecimalValue.substring(i, i + 2);
			output.append((char) Integer.parseInt(str, 16));
		}
		return hexadecimalValue;
	}

	public static void main(String[] args) throws UnsupportedEncodingException {
		System.out.println(getHexaDecimalValue(getEncodedInput(getReverseString("ajay"))));
	}
}
